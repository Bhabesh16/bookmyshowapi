module.exports = (mongoose) => {
    let schema = mongoose.Schema(
        {
            title: String,
            description: String,
            place: String,
            date: String,
            image: String,
            backgroundImage: String
        },
        { timestamps: true }
    );

    schema.method("toJSON", function () {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Event = mongoose.model("event", schema);
    return Event;
};