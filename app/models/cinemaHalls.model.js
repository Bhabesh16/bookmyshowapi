module.exports = (mongoose) => {
    let schema = mongoose.Schema(
        {
            title: String,
            location: String,
            moviesTime: Object,
            date: String,
            seats: Object
        },
        { timestamps: true }
    );

    schema.method("toJSON", function () {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const CinemaHall = mongoose.model("cinemahall", schema);
    return CinemaHall;
};