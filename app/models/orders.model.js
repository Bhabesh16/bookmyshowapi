module.exports = (mongoose) => {
    let schema = mongoose.Schema(
        {
            userId: String,
            title: String,
            language: String,
            date: String,
            time: String,
            cinema: String,
            seats: Array
        },
        { timestamps: true }
    );

    schema.method("toJSON", function () {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Order = mongoose.model("order", schema);
    return Order;
};