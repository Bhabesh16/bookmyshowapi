const db = require("../models");
const Movie = db.movies;

exports.create = (req, res, next) => {
    const { title, description, places, type, date, image, backgroundImage, rating, language } = req.body;

    if (!title || !description || !places || !date || !image) {
        next({
            status: 400,
            message: "Content can not be empty! required data title, description, places, date, image"
        });
        return;
    } else {

        const movie = new Movie({
            title,
            description,
            places,
            type,
            date,
            language,
            image,
            backgroundImage,
            rating
        });

        movie
            .save(movie)
            .then((data) => {
                res.send(data);
            })
            .catch((err) => {
                next({
                    status: 500,
                    message: err.message || "Some error occurred while creating the Movies."
                });
            });
    }
};

exports.findAll = (req, res, next) => {
    const title = req.query.title;

    let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

    Movie.find(condition)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while retrieving movies."
            });
        });
};

exports.findOne = (req, res, next) => {
    const id = req.params.id;

    Movie.findById(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: "Not found Movie with id " + id
                });
            } else {
                res.send(data);
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error retrieving Movie with id=" + id
            });
        });
};

exports.update = (req, res, next) => {
    if (!req.body) {
        return next({
            status: 400,
            message: "Data to update can not be empty!"
        });

    }

    const id = req.params.id;

    Movie.findByIdAndUpdate(id, req.body)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot update Movie with id=${id}. Maybe Movie was not found!`
                });
            } else {
                res.send({ message: "Movie was updated successfully." });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error updating Movie with id=" + id
            });
        });
};

exports.delete = (req, res, next) => {
    const id = req.params.id;

    Movie.findByIdAndRemove(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot delete Movie with id=${id}. Maybe Movie was not found!`
                });
            } else {
                res.send({
                    message: "Movie was deleted successfully!"
                });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Could not delete Movie with id=" + id
            });
        });
};

exports.deleteAll = (req, res, next) => {
    Movie.deleteMany({})
        .then((data) => {
            res.send({
                message: `${data.deletedCount} Movies were deleted successfully!`
            });
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while removing all movies."
            });
        });
};