const db = require("../models");
const Event = db.events;

exports.create = (req, res, next) => {
    const { title, description, place, date, image, backgroundImage } = req.body;

    if (!title || !description || !place || !date || !image) {

        next({
            status: 400,
            message: "Content can not be empty! required data title, description, place, date, image"
        });
        return;
    } else {

        const event = new Event({
            title,
            description,
            place,
            date,
            image,
            backgroundImage
        });

        event
            .save(event)
            .then((data) => {
                res.send(data);
            })
            .catch((err) => {
                next({
                    status: 500,
                    message: err.message || "Some error occurred while creating the Events."
                });
            });
    }
};

exports.findAll = (req, res, next) => {
    const title = req.query.title;

    let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

    Event.find(condition)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while retrieving events."
            });
        });
};

exports.findOne = (req, res, next) => {
    const id = req.params.id;

    Event.findById(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: "Not found Event with id " + id
                });
            } else {
                res.send(data);
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error retrieving Event with id=" + id
            });
        });
};

exports.update = (req, res, next) => {
    if (!req.body) {

        return next({
            status: 400,
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Event.findByIdAndUpdate(id, req.body)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot update Event with id=${id}. Maybe Event was not found!`
                });
            } else {
                res.send({ message: "Event was updated successfully." });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error updating Event with id=" + id
            });
        });
};

exports.delete = (req, res, next) => {
    const id = req.params.id;

    Event.findByIdAndRemove(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot delete Event with id=${id}. Maybe Event was not found!`
                });
            } else {
                res.send({
                    message: "Event was deleted successfully!"
                });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Could not delete Event with id=" + id
            });
        });
};

exports.deleteAll = (req, res, next) => {
    Event.deleteMany({})
        .then((data) => {
            res.send({
                message: `${data.deletedCount} Events were deleted successfully!`
            });
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while removing all events."
            });
        });
};