const express = require("express");

const movies = require("../controllers/movie.controller.js");

module.exports = (app) => {

    const router = express.Router();

    router.post("/", movies.create);

    router.get("/", movies.findAll);

    router.get("/:id", movies.findOne);

    router.put("/:id", movies.update);

    router.delete("/:id", movies.delete);

    router.delete("/", movies.deleteAll);

    app.use('/api/movies', router);
};