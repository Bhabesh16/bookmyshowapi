const express = require("express");

const events = require("../controllers/event.controller.js");

module.exports = (app) => {

    const router = express.Router();

    router.post("/", events.create);

    router.get("/", events.findAll);

    router.get("/:id", events.findOne);

    router.put("/:id", events.update);

    router.delete("/:id", events.delete);

    router.delete("/", events.deleteAll);

    app.use('/api/events', router);
};